﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;

namespace FizzBuzz.Business.UnitTests
{
    [TestClass]
    public class FizzBuzzServiceTests
    {

        [TestMethod]
        public void GetRecordSet_WithNoLimit_ReturnsEmpty()
        {
            var emptyList = new List<string>();
            var service = new FizzBuzzService(true);

            var records = service.GetRecordSet(0);

            Assert.AreEqual(records.Count, emptyList.Count);
        }

        [TestMethod]
        public void GetRecordSet_WithFizzBussRule_ReturnsBuzzResult()
        {
            var isWednesday = false;
            var range = 5;
            var expactedResult = new List<string> { "1", "2", "Fizz", "4", "Buzz" };
            var service = new FizzBuzzService(isWednesday);

            var records = service.GetRecordSet(range);

            Assert.AreEqual(records.Count, range);
            CollectionAssert.AreEqual(records.ToList(), expactedResult);
        }
    }
}
