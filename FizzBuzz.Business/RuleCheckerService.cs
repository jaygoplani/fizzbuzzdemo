﻿using FizzBuzz.Business.Interface;
using FizzBuzz.Business.Rules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz.Business
{
    public class RuleCheckerService : IRuleCheckerService
    {
        public IRule GetMatchedRule(int inputNumber)
        {
            var rules = new List<IRule> { new FizzBuzzRule(), new FizzRule(), new BuzzRule() };
            foreach (var rule in rules)
            {
                if (rule.IsMatched(inputNumber))
                {
                    return rule;
                }
            }
            return null;
        }
    }
}
