﻿using FizzBuzz.Business.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz.Business.Rules
{
    public class FizzRule : IRule
    {
        public bool IsMatched(int Number)
        {
            return Number % 3 == 0;
        }

        public string Print(bool isWednesday)
        {

            return isWednesday ? "Wizz" : "Fizz";

        }
    }
}
