﻿using FizzBuzz.Business.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz.Business.Rules
{
    public class FizzBuzzRule : IRule
    {
        public bool IsMatched(int Number)
        {
            return Number % 3 == 0 && Number % 5 == 0;
        }

        public string Print(bool isWednesday)
        {
            return isWednesday ? "WizzWuzz" : "FizzBuzz";
        }
    }
}
