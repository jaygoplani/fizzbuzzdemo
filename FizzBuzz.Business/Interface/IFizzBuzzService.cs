﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz.Business.Interface
{
    public  interface IFizzBuzzService
    {
        List<string> GetRecordSet(int inputNumber);
    }
}
