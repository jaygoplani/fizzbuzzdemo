﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz.Business.Interface
{
    public interface IRule
    {
        bool IsMatched(int inputNumber);

        string Print(bool isWednesday);
    }
}
