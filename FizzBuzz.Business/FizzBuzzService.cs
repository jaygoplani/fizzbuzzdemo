﻿using FizzBuzz.Business.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FizzBuzz.Business
{
    public class FizzBuzzService: IFizzBuzzService
    {
        private bool _isWednesday { get; set; }
        public FizzBuzzService(bool isWednesday)
        {
            _isWednesday = isWednesday;
        }
        public List<string> GetRecordSet(int inputNumber)
        {
            var ruleService = new RuleCheckerService();
            var records = new List<string>();

            for (int i = 1; i <= inputNumber; i++)
            {
                var rules = ruleService.GetMatchedRule(i);
                if (rules != null)
                {
                    records.Add(rules.Print(_isWednesday));
                }
                else
                {
                    records.Add(i.ToString());
                }
            }

            return records;

        }
    }
}
