﻿using FizzBuzz.Web;
using FizzBuzz.Web.Controllers;
using FizzBuzz.Web.Models;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.Mvc;

namespace FizzBuzz.Web.UnitTests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void Index()
        {
            // Arrange
            HomeController controller = new HomeController();
            FizzBuzzViewModel fizzBuzzViewModel = new FizzBuzzViewModel
            {
                InputNumber = 3,
                Records = null
            };

            // Act
            ViewResult result = controller.Index(fizzBuzzViewModel, 1) as ViewResult;

            // Assert
            Assert.IsNotNull(result);
        }
    }
}
