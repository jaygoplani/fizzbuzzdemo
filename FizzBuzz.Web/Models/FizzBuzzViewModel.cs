﻿using PagedList;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FizzBuzz.Web.Models
{
    public class FizzBuzzViewModel
    {
        [Required]
        [RegularExpression("([0-9]+)", ErrorMessage = "Please enter valid Number")]
        [Range(0, 1000, ErrorMessage = "The value must be between {1} and {2}")]
        public int InputNumber { get; set; }

        public PagedList<string> Records { get; set; }
    }
}