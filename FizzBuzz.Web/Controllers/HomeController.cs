﻿using FizzBuzz.Business;
using FizzBuzz.Web.Models;
using PagedList;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FizzBuzz.Web.Controllers
{
    public class HomeController : Controller
    {
        public bool isWednesday = Convert.ToBoolean(ConfigurationManager.AppSettings["IsWednesday"]);
        public const int PageSize = 20;

        public ActionResult Index(FizzBuzzViewModel fizzBuzzViewModel, int? page)
        {
            var service = new FizzBuzzService(isWednesday);

            int pageNumber = page ?? 1;

            if (!(fizzBuzzViewModel.InputNumber > 0 && fizzBuzzViewModel.InputNumber <= 1000))
            {
                ModelState.AddModelError("InputNumber", "Please enter valid Number");
            }


            if (ModelState.IsValid)
            {
                fizzBuzzViewModel.Records = (PagedList<string>)service.GetRecordSet(fizzBuzzViewModel.InputNumber).ToPagedList(pageNumber, PageSize);
            }

            return View(model: fizzBuzzViewModel);
        }
       
    }
}